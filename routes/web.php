<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** @noinspection PhpUndefinedMethodInspection */
Auth::routes();

Route::group([
    'prefix' => '/home',
    'namespace' => 'Cabinet',
    'middleware' => ['auth', 'only_user'],
    'as' => 'home.',
], function(){
    Route::get('/', 'DefaultController@index')->name("index");

    Route::group([
        'prefix' => '/settings',
        'as' => 'settings.',
    ], function(){
        Route::get('/', 'SettingsController@index')->name('index');
        Route::post('/save', 'SettingsController@save')->name('save');
    });

    Route::group([
        'prefix' => '/robots',
        'as' => 'robots.',
    ], function(){
        Route::get('/add', 'RobotsController@add')->name('add');
        Route::post('/store', 'RobotsController@store')->name('store');
        Route::get('/{id}/edit', 'RobotsController@edit')->name('edit');
        Route::post('/{id}/change', 'RobotsController@change')->name('change');
        Route::get('/{id}/delete', 'RobotsController@delete')->name('delete');
    });

    Route::group([
        'prefix' => '/commands',
        'as' => 'commands.',
    ], function(){
        Route::get('/', 'CommandsController@index')->name('index');
        Route::get('/add', 'CommandsController@add')->name('add');
        Route::post('/store', 'CommandsController@store')->name('store');
        Route::get('/{command_id}/edit', 'CommandsController@edit')->name('edit');
        Route::post('/{command_id}/change', 'CommandsController@change')->name('change');
        Route::get('/{command_id}/delete', 'CommandsController@delete')->name('delete');
    });

    Route::group([
        'prefix' => '/movements',
        'as' => 'movements.',
    ], function(){
        Route::get('/', 'MovementsController@index')->name('index');
        Route::get('/add', 'MovementsController@add')->name('add');
        Route::post('/store', 'MovementsController@store')->name('store');
        Route::get('/{movement_id}/edit', 'MovementsController@edit')->name('edit');
        Route::post('/{movement_id}/change', 'MovementsController@change')->name('change');
        Route::get('/{movement_id}/delete', 'MovementsController@delete')->name('delete');
    });

    Route::group([
        'prefix' => '/audios',
        'as' => 'audios.',
    ], function(){
        Route::get('/', 'AudioController@index')->name('index');
        Route::get('/add', 'AudioController@add')->name('add');
        Route::post('/store', 'AudioController@store')->name('store');
        Route::get('/{id}/edit', 'AudioController@edit')->name('edit');
        Route::post('/{id}/change', 'AudioController@change')->name('change');
        Route::get('/{id}/delete', 'AudioController@delete')->name('delete');
    });
});

Route::group([
    'prefix' => '/admin',
    'namespace' => 'Admin',
    'middleware' => 'admin',
    'as' => 'admin.',
], function() {
    Route::get('/', 'DefaultController@index')->name('index');

    Route::get('/commands', 'DefaultController@commands')->name('commands');
    Route::get('/commands/add', 'DefaultController@addCommand')->name('command.add');
    Route::post('/commands/store', 'DefaultController@storeCommand')->name('command.store');
    Route::get('/commands/{id}/edit', 'DefaultController@editCommand')->name('command.edit');
    Route::post('/commands/{id}/change', 'DefaultController@changeCommand')->name('command.change');
    Route::get('/commands/{id}/delete', 'DefaultController@deleteCommand')->name('command.delete');

    Route::get('/movements', 'DefaultController@movements')->name('movements');
    Route::get('/movements/add', 'DefaultController@addMovement')->name('movement.add');
    Route::post('/movements/store', 'DefaultController@storeMovement')->name('movement.store');
    Route::get('/movements/{id}/edit', 'DefaultController@editMovement')->name('movement.edit');
    Route::post('/movements/{id}/change', 'DefaultController@changeMovement')->name('movement.change');
    Route::get('/movements/{id}/delete', 'DefaultController@deleteMovement')->name('movement.delete');

    Route::group([
        'prefix' => '/audios',
        'as' => 'audios.',
    ], function(){
        Route::get('/', 'AudioController@index')->name('index');
        Route::get('/add', 'AudioController@add')->name('add');
        Route::post('/store', 'AudioController@store')->name('store');
        Route::get('/{id}/edit', 'AudioController@edit')->name('edit');
        Route::post('/{id}/change', 'AudioController@change')->name('change');
        Route::get('/{id}/delete', 'AudioController@delete')->name('delete');
    });

    Route::group([
        'prefix' => '/missed-commands',
        'as' => 'missed_commands.',
    ], function(){
        Route::get('/', 'MissedCommandsController@index')->name('index');
        Route::get('/add', 'MissedCommandsController@add')->name('add');
        Route::post('/store', 'MissedCommandsController@store')->name('store');
        Route::get('/delete-all', 'MissedCommandsController@deleteAll')->name('delete_all');
        Route::get('/{id}/delete', 'MissedCommandsController@delete')->name('delete');
    });

    Route::group([
        'prefix' => '/updates',
        'as' => 'updates.',
    ], function(){
        Route::get('/', 'UpdatesController@index')->name('index');
        Route::get('/add', 'UpdatesController@add')->name('add');
        Route::post('/store', 'UpdatesController@store')->name('store');
        Route::get('/{id}/delete', 'UpdatesController@delete')->name('delete');
    });

    Route::group([
        'prefix' => '/users',
        'as' => 'users.',
    ], function(){
        Route::get('/', 'UsersController@index')->name('index');
        Route::get('/{id}', 'UsersController@detail')->name('detail');
        Route::get('/{id}/robots', 'UsersController@robots')->name('robots');
        Route::get('/{id}/commands', 'UsersController@commands')->name('commands');
        Route::get('/{id}/movements', 'UsersController@movements')->name('movements');
    });
});