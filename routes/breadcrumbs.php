<?php

use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;

Breadcrumbs::register('login', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Авторизация');
});
Breadcrumbs::register('password.request', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Запрос пароля');
});
Breadcrumbs::register('password.reset', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Сброс пароля');
});
Breadcrumbs::register('register', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Регистрация');
});

require(__DIR__."/breadcrumbs/home.php");
require(__DIR__."/breadcrumbs/admin.php");