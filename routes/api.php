<?php

/**
 * TODO: refresh, logout
 */
Route::any('/login', "ApiController@login");

Route::group([
    'prefix' => '/robots',
    'middleware' => 'jwt',
    'namespace' => 'Api'
], function(){
    Route::get('/', 'RobotController@index');
    Route::post('/', 'RobotController@create');
    Route::delete('/{id}', 'RobotController@delete');
});

Route::group([
    'prefix' => '/commands',
    'middleware' => 'jwt',
    'namespace' => 'Api'
], function(){
    Route::get('/', 'CommandController@index');
    Route::post('/bulk', 'CommandController@storeCommands');
    Route::delete('/{id}', 'CommandController@delete');
});

Route::group([
    'prefix' => '/movements',
    'middleware' => 'jwt',
    'namespace' => 'Api'
], function(){
    Route::get('/', 'MovementController@index');
    Route::post('/', 'MovementController@store');
    Route::delete('/{id}', 'MovementController@delete');
});

Route::get('/search', 'ApiController@search');
Route::get('/token', 'ApiController@getToken');
Route::get('/robot-commands', 'ApiController@getCommandsFromRobot');
Route::get('/robot-movements', 'ApiController@getMovementsFromRobot');
Route::post('/movements/store', 'ApiController@storeMovement');

Route::get('/poses', 'ApiController@poses');
Route::post('/missed_commands/store', 'ApiController@storeMissedCommand');

Route::get('/updates', 'ApiController@updates');
Route::get('/audios', 'ApiController@audios');