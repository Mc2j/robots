<?php

use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;

Breadcrumbs::register('home.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->push('Главная', route('home.index'));
});

Breadcrumbs::register('home.robots.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Добавить робота');
});
Breadcrumbs::register('home.robots.edit', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Редактировать робота');
});
Breadcrumbs::register('home.robots.commands', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Команды робота');
});
Breadcrumbs::register('home.robots.movements', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Движения робота');
});

Breadcrumbs::register('home.commands.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Список ваших диалогов', route('home.commands.index'));
});
Breadcrumbs::register('home.commands.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.commands.index');
    $breadcrumbs->push('Добавить диалог');
});
Breadcrumbs::register('home.commands.edit', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.commands.index');
    $breadcrumbs->push('Редактировать диалог');
});

Breadcrumbs::register('home.movements.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Список ваших движений', route('home.movements.index'));
});
Breadcrumbs::register('home.movements.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.movements.index');
    $breadcrumbs->push('Добавить движение');
});
Breadcrumbs::register('home.movements.edit', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.movements.index');
    $breadcrumbs->push('Редактировать движение');
});

Breadcrumbs::register('home.audios.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Список аудиофайлов', route('home.audios.index'));
});
Breadcrumbs::register('home.audios.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.audios.index');
    $breadcrumbs->push('Добавить аудиофайл');
});
Breadcrumbs::register('home.audios.edit', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.audios.index');
    $breadcrumbs->push('Редактировать аудиофайл');
});

Breadcrumbs::register('home.settings.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Настройки');
});