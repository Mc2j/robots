<?php

use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;

Breadcrumbs::register('admin.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('home.index');
    $breadcrumbs->push('Панель админстриования', route('admin.index'));
});

Breadcrumbs::register('admin.commands', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Список общих команд', route('admin.commands'));
});
Breadcrumbs::register('admin.command.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.commands');
    $breadcrumbs->push('Добавить команду');
});
Breadcrumbs::register('admin.command.edit', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.commands');
    $breadcrumbs->push('Редактировать команду');
});

Breadcrumbs::register('admin.movements', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Список общих движений', route('admin.movements'));
});
Breadcrumbs::register('admin.movement.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.movements');
    $breadcrumbs->push('Добавить движение');
});
Breadcrumbs::register('admin.movement.edit', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.movements');
    $breadcrumbs->push('Редактировать движение');
});

Breadcrumbs::register('admin.audios.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Список аудиофайлов', route('admin.audios.index'));
});
Breadcrumbs::register('admin.audios.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.audios.index');
    $breadcrumbs->push('Добавить аудиофайл');
});
Breadcrumbs::register('admin.audios.edit', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.audios.index');
    $breadcrumbs->push('Редактировать аудиофайл');
});

Breadcrumbs::register('admin.missed_commands.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Диалоги без ответа', route("admin.missed_commands.index"));
});
Breadcrumbs::register('admin.missed_commands.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.missed_commands.index');
    $breadcrumbs->push('Добавить ответ в диалог');
});

Breadcrumbs::register('admin.users.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Пользователи', route("admin.users.index"));
});
Breadcrumbs::register('admin.users.detail', function (BreadcrumbsGenerator $breadcrumbs, $id) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Просмотр пользователя', route("admin.users.detail", ['id' => $id]));
});
Breadcrumbs::register('admin.users.robots', function (BreadcrumbsGenerator $breadcrumbs, $id) {
    $breadcrumbs->parent('admin.users.detail', $id);
    $breadcrumbs->push('Роботы пользователя');
});
Breadcrumbs::register('admin.users.commands', function (BreadcrumbsGenerator $breadcrumbs, $id) {
    $breadcrumbs->parent('admin.users.detail', $id);
    $breadcrumbs->push('Диалоги пользователя');
});
Breadcrumbs::register('admin.users.movements', function (BreadcrumbsGenerator $breadcrumbs, $id) {
    $breadcrumbs->parent('admin.users.detail', $id);
    $breadcrumbs->push('Движения пользователя');
});

Breadcrumbs::register('admin.updates.index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Список обновлений', route('admin.updates.index'));
});
Breadcrumbs::register('admin.updates.add', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('admin.updates.index');
    $breadcrumbs->push('Добавить обновление');
});