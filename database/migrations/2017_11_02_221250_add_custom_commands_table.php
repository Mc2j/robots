<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_commands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('input');
            $table->string('output');
            $table->integer('vocalizer');
            $table->integer('robot_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('custom_commands');
    }
}
