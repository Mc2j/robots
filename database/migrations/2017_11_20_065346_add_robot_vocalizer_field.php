<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRobotVocalizerField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('robots', function (Blueprint $table) {
            $table->integer('vocalizer')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('vocalizer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('robots', function (Blueprint $table) {
            $table->dropColumn('vocalizer');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->integer('vocalizer')->nullable();
        });
    }
}
