<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRobotToUserField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commands', function (Blueprint $table) {
            $table->dropColumn('robot_id');
            $table->integer('user_id');
        });
        Schema::table('movements', function (Blueprint $table) {
            $table->dropColumn('robot_id');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commands', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->integer('robot_id');
        });
        Schema::table('movements', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->integer('robot_id');
        });
    }
}
