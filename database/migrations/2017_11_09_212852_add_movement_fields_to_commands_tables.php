<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMovementFieldsToCommandsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commands', function (Blueprint $table) {
            $table->string('movement_type')->nullable();
            $table->integer('movement_id')->nullable();
        });
        Schema::table('custom_commands', function (Blueprint $table) {
            $table->string('movement_type')->nullable();
            $table->integer('movement_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commands', function (Blueprint $table) {
            $table->dropColumn('movement_type');
            $table->dropColumn('movement_id');
        });
        Schema::table('custom_commands', function (Blueprint $table) {
            $table->dropColumn('movement_type');
            $table->dropColumn('movement_id');
        });
    }
}
