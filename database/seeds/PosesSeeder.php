<?php

use App\Models\Pose;
use Illuminate\Database\Seeder;

class PosesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root = __DIR__."/poses/";
        $dirs = scandir($root);
        foreach ($dirs as $dir_name) {
            if (in_array($dir_name, [".", ".."])) {
                continue;
            }

            $file = $root.$dir_name."/Play.jn";
            if (file_exists($file)) {
                $json = json_decode(file_get_contents($file));
                if ($json) {
                    foreach($json->DZList as $movement) {

                        $play_time = $movement->PlayTime;
                        $poses = [];

                        usort($movement->XiaFa, [$this, 'sortByEngineId']);
                        foreach ($movement->XiaFa as $XiaFa) {
                            $poses[] = [
                                'Id' => $XiaFa->Id,
                                'Value' => $XiaFa->Value,
                            ];
                        }

                        $poses_json = json_encode($poses);
                        $pose = Pose::where("PlayTime", '=', $play_time)->where("XiaFa", '=', $poses_json)->first();

                        if ($pose) {
                            continue;
                        }

                        $pose = new Pose([
                            'PlayTime' => $play_time,
                            'XiaFa' => $poses_json,
                        ]);
                        $pose->save();
                    }
                }
            }
        }
    }

    protected function sortByEngineId($a, $b)
    {
        return $a->Id <=> $b->Id;
    }
}
