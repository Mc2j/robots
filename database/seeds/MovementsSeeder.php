<?php

use App\Models\Movement;
use Illuminate\Database\Seeder;

class MovementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root = __DIR__."/movements/";
        $dirs = scandir($root);
        foreach ($dirs as $dir_name) {
            if (in_array($dir_name, [".", ".."])) {
                continue;
            }

            $file = $root.$dir_name."/".$dir_name.".zip";
            if (file_exists($file)) {
                $zip_file = file_get_contents($file);
                if (mb_strlen($zip_file) > 0) {
                    Storage::put("public/movements/".$dir_name.'.erf', $zip_file, 'public');

                    $movement = new Movement([
                        'name' => $dir_name,
                        'file' => '/storage/movements/'.$dir_name.".erf",
                    ]);
                    $movement->save();
                }
            }
        }
    }
}
