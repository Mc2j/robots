<?php

use App\Models\Command;
use Illuminate\Database\Seeder;

class Commands extends Seeder
{
    protected $commands = [
        ['input' => 'как тебя зовут', 'output' => 'меня зовут макс я робот', 'vocalizer' => 2],
        ['input' => 'какая сегодня погода', 'output' => 'погода сегодня супер', 'vocalizer' => 0],
        ['input' => 'сколько тебе лет', 'output' => 'мне только один год', 'vocalizer' => 1],
        ['input' => 'макс я занят', 'output' => 'ты делаешь уроки?', 'vocalizer' => 5],
        ['input' => 'математика еще не готова', 'output' => 'хочешь я тебе помогу?', 'vocalizer' => 3],
        ['input' => 'спасибо макс', 'output' => 'рад тебе помочь', 'vocalizer' => 4],
        ['input' => 'только не говори маме', 'output' => 'мой рот на замочке', 'vocalizer' => 3],
        ['input' => 'тебя можно погладить', 'output' => 'да но очень аккуратно', 'vocalizer' => 4],
        ['input' => 'погода', 'output' => 'ниченго хорошего', 'vocalizer' => 4],
    ];

    public function run()
    {
        foreach ($this->commands as $command) {
            $model = new Command();
            $model->fill($command);
            $model->save();
        }
    }
}
