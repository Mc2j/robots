<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        $user = new User();
        $user->fill([
            'name' => 'Admin',
            'email' => 'admin@easyrobot.online',
            'password' => Hash::make(env('ADMIN_PASSWORD')),
        ]);

        $user->save();
    }
}
