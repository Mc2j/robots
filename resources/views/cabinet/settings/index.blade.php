@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">
                        Настройки
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" action="{{route("home.settings.save")}}">
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
