@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Редактирование аудиофайла</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{route("home.audios.change", ['id' => $audio->id])}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="audio_name" class="col-sm-2 control-label">Имя:</label>
                                <div class="col-sm-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="audio_name"
                                            name="Audio[name]"
                                            placeholder="Имя"
                                            value="{{old('name', $audio->name)}}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="audio_file" class="col-sm-2 control-label">Файл:</label>
                                <div class="col-sm-10">
                                    Старый аудиофайл: <a href="{{$audio->file}}">Скачать</a><br>
                                    <input type="file" id="audio_file" name="Audio[file]">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="{{route('home.audios.index')}}" class="btn btn-default">Отмена</a>
                                    <button type="submit" class="btn btn-default">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection