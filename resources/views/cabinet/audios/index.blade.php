@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Ваши аудиофайлы</div>
                    <div class="panel-body">
                        <a class="btn btn-primary btn-sm" href ="{{route('home.audios.add')}}">Добавить аудиофайл</a>
                        <br>
                        <br>
                        @if($custom_audios->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Имя</th>
                                    <th>Файл</th>
                                    <th>Действия</th>
                                </tr>
                                @foreach($custom_audios as $audio)
                                <tr>
                                    <td>{{$audio->name}}</td>
                                    <td>
                                        <a href="{{$audio->file}}">Скачать</a>
                                    </td>
                                    <td style="width: 200px;">
                                        <a class="btn btn-default btn-sm" href = "{{route('home.audios.edit', ['id' => $audio->id])}}">Редактировать</a>
                                        <a class="btn btn-danger btn-sm" href = "{{route('home.audios.delete', ['id' => $audio->id])}}">Удалить</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                    <div class="panel-heading lead">Общие аудиофайлы</div>
                    <div class="panel-body">
                        @if($audios->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Имя</th>
                                    <th>Файл</th>
                                </tr>
                                @foreach($audios as $audio)
                                <tr>
                                    <td>{{$audio->name}}</td>
                                    <td>
                                        <a href="{{$audio->file}}">Скачать</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
