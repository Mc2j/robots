@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    {{--<div class="panel-heading">Диалог "{{$command->input}}"</div>--}}
                    <div class="panel-heading lead">Редактирование Диалога</div>
                    @if (!$errors->isEmpty())
                        <div class = "error">
                        @foreach($errors->all() as $error)
                            {{$error}} <br>
                        @endforeach
                        </div>
                    @endif
                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="post" action="{{route("home.commands.change", ['id' => $command->id])}}">
                            {{csrf_field()}}
                            <input id = "command_vocalizer" type = "hidden" name = "Command[vocalizer]" value = "{{old('vocalizer', $command->vocalizer)}}">

                            <div class="form-group">
                                <label for="command_input" class="col-sm-2 control-label">Входящая команда:</label>
                                <div class="col-sm-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="command_input"
                                            name="Command[input]"
                                            placeholder="Входящая команда:"
                                            value="{{old('input', $command->input)}}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="command_output" class="col-sm-2 control-label">Ответ:</label>
                                <div class="col-sm-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="command_output"
                                            name="Command[output]"
                                            placeholder="Входящая команда:"
                                            value="{{old('output', $command->output)}}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="command_movement" class="col-sm-2 control-label">Движение:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id = "command_movement" name = "Command[movement_id]">
                                        <option value = 0>-</option>
                                        @foreach ($movements as $movement)
                                            @php $selected = ($movement->id == old('movement_id', $command->movement_id)) ? "selected = 'selected'" : "" @endphp
                                            <option value = {{$movement->id}} {{$selected}}>{{$movement->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="/home/commands" class="btn btn-default">Отмена</a>
                                    <button type="submit" class="btn btn-default">Сохранить</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
