@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Ваши диалоги</div>
                    <div class="panel-body">
                        <a class="btn btn-primary btn-sm" href ="{{route('home.commands.add')}}">Добавить Диалог</a>
                        <br>
                        <br>
                        @if($custom_commands->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Команда</th>
                                    <th>Ответ</th>
                                    <th>Движение</th>
                                    <th>Действия</th>
                                </tr>
                                @foreach($custom_commands as $command)
                                <tr>
                                    <td>{{$command->input}}</td>
                                    <td>{{$command->output}}</td>
                                    <td>
                                        @if ($command->movement)
                                            {{$command->movement->name}}
                                        @endif
                                    </td>
                                    <td style="width: 200px;" >
                                        <a class="btn btn-default btn-sm" href = "{{route('home.commands.edit', ['id' => $command->id])}}">Редактировать</a>
                                        <a class="btn btn-danger btn-sm" href = "{{route('home.commands.delete', ['id' => $command->id])}}">Удалить</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                    <div class="panel-heading lead">Общие диалоги</div>
                    <div class="panel-body">
                        @if($commands->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Команда</th>
                                    <th>Ответ</th>
                                    <th>Движение</th>
                                </tr>
                                @foreach($commands as $command)
                                    <tr>
                                        <td>{{$command->input}}</td>
                                        <td>{{$command->output}}</td>
                                        <td>
                                            @if ($command->movement)
                                                {{$command->movement->name}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection