@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Редактирование движения</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data"
                              action="{{route("home.movements.change", ['id' => $movement->id])}}">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="movement_name" class="col-sm-2 control-label">Название команды:</label>
                                <div class="col-sm-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="movement_name"
                                            name="Movement[name]"
                                            placeholder="Входящая команда:"
                                            value="{{old('name', $movement->name)}}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="movement_file" class="col-sm-2 control-label">Файл движения:</label>
                                <div class="col-sm-10">
                                    Старое движение: <a href="{{$movement->file}}">Скачать</a><br>
                                    <input type="file" id="movement_file" name="Movement[file]">
                                    <p class="help-block">Файл движения формата *.erf</p>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="/home/movements" class="btn btn-default">Отмена</a>
                                    <button type="submit" class="btn btn-default">Сохранить</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection