@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Ваши движения</div>
                    <div class="panel-body">
                        <a class="btn btn-primary btn-sm" href ="{{route('home.movements.add')}}">Добавить Движение</a>
                        <br>
                        <br>
                        @if($custom_movements->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Имя</th>
                                    <th>Файл</th>
                                    <th>Действия</th>
                                </tr>
                                @foreach($custom_movements as $movement)
                                <tr>
                                    <td>{{$movement->name}}</td>
                                    <td>
                                        <a href="{{$movement->file}}">Скачать</a>
                                    </td>
                                    <td style="width: 200px;">
                                        <a class="btn btn-default btn-sm" href = "{{route('home.movements.edit', ['id' => $movement->id])}}">Редактировать</a>
                                        <a class="btn btn-danger btn-sm" href = "{{route('home.movements.delete', ['id' => $movement->id])}}">Удалить</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                    <div class="panel-heading lead">Общие движения</div>
                    <div class="panel-body">
                        @if($movements->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Имя</th>
                                    <th>Файл</th>
                                </tr>
                                @foreach($movements as $movement)
                                <tr>
                                    <td>{{$movement->name}}</td>
                                    <td>
                                        <a href="{{$movement->file}}">Скачать</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
