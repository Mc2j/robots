@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Редактировать робота {{$robot->imei}}</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post"
                              action="{{route("home.robots.change", ['id' => $robot->id])}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="robot_token" class="col-sm-2 control-label">Imei робота:</label>
                                <div class="col-sm-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="robot_token"
                                            name="Robot[imei]"
                                            placeholder="Укажите imei робота"
                                            value="{{old('imei', $robot->imei)}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="robot_vocalizer" class="col-sm-2 control-label">Голос:</label>
                                <div class="col-sm-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="robot_vocalizer"
                                            name="Robot[vocalizer]"
                                            placeholder="Укажите голос робота"
                                            value="{{old('vocalizer', $robot->vocalizer)}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="robot_hello_message" class="col-sm-2 control-label">Слово активации:</label>
                                <div class="col-sm-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            id="robot_hello_message"
                                            name="Robot[hello_message]"
                                            placeholder="Укажите слово активации робота"
                                            value="{{old('hello_message', $robot->hello_message)}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="{{route('home.index')}}" class="btn btn-default">Отмена</a>
                                    <button type="submit" class="btn btn-default">Обновить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
