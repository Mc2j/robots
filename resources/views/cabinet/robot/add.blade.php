@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Добавить робота</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" action="{{route("home.robots.store")}}">
                            {{csrf_field()}}
                            @if (!$errors->isEmpty())
                                <div class="form-group has-error">
                                    @else
                                        <div class="form-group">
                                            @endif
                                            <label for="robot_token" class="col-sm-2 control-label">Imei робота:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="robot_token"
                                                       name="Robot[imei]" placeholder="Укажите imei робота"
                                                       value="{{old('imei')}}">
                                                @if (!$errors->isEmpty())
                                                    @foreach($errors->all() as $error)
                                                        <span class="help-block">{{$error}}</span>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="robot_vocalizer" class="col-sm-2 control-label">Голос:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="robot_vocalizer" name="Robot[vocalizer]" placeholder="Укажите голос робота" value="{{old('vocalizer')}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="robot_hello_message" class="col-sm-2 control-label">Слово активации:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="robot_hello_message" name="Robot[hello_message]" placeholder="Укажите слово активации робота" value="{{old('hello_message')}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <a href="{{route('home.index')}}" class="btn btn-default">Отмена</a>
                                                <button type="submit" class="btn btn-default">Сохранить</button>
                                            </div>
                                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
