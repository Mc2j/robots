@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">
                        Личный кабинет
                    </div>
                    <div class="panel-body">
                        <!--a class="btn btn-primary btn-sm" href="{{route('home.settings.index')}}">Настройки</a>
                        <br><br-->
                        <a class="btn btn-primary btn-sm" href="{{route('home.robots.add')}}">Добавить робота</a>
                        <a class="btn btn-primary btn-sm" href="{{route('home.commands.index')}}">Доступные диалоги</a>
                        <a class="btn btn-primary btn-sm" href="{{route('home.movements.index')}}">Доступные движения</a>
                        <a class="btn btn-primary btn-sm" href="{{route('home.audios.index')}}">Аудиофайлы</a>
                        @if ($robots->count() == 0)
                                <br><br>
                                <p class="lead">У вас нет роботов</p>
                        @else
                            <p style="padding-top: 20px" class="lead">Список ваших роботов</p>
                            <table width='100%' class="table">
                                <tr>
                                    <th>Робот: imei</th>
                                    <th style="width: 300px;">Действия</th>
                                </tr>
                                @foreach($robots as $robot)
                                    <tr>
                                        <td>{{$robot->imei}}</td>
                                        <td style="width: 300px;">
                                            <a class="btn btn-default btn-sm"
                                               href="{{route('home.robots.edit', ['id' => $robot->id])}}">Редактировать
                                                Робота</a>
                                            <a class="btn btn-danger btn-sm"
                                               href="{{route('home.robots.delete', ['id' => $robot->id])}}">Удалить
                                                Робота</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
