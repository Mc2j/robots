@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Общие команды</div>
                    <div class="panel-body">
                        <a class="btn btn-primary btn-sm" href ="{{route('admin.command.add')}}">Добавить Базовый Диалог</a>
                        <br>
                        <br>
                        @if ($commands->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Входящая команда</th>
                                    <th>Ответ</th>
                                    <th>Движение</th>
                                    <th style="width: 200px;">Действия</th>
                                </tr>
                                @foreach($commands as $command)
                                <tr>
                                    <td>{{$command->input}}</td>
                                    <td>{{$command->output}}</td>
                                    <td>
                                        @if ($command->movement)
                                            {{$command->movement->name}}
                                        @endif
                                    </td>
                                    <td style="width: 200px;" >
                                        <a class="btn btn-default btn-sm" href = "{{route('admin.command.edit', ['id' => $command->id])}}">Редактировать</a>
                                        <a class="btn btn-danger btn-sm" href = "{{route('admin.command.delete', ['id' => $command->id])}}">Удалить</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
