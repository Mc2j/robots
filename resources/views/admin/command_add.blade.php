@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Добавить команду</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form method="POST" action="{{route("admin.command.store")}}" role="form"
                              class="form-horizontal">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="command_input" class="col-sm-2 control-label">Входящая команда:</label>
                                <div class="col-sm-10">
                                    <input id="command_input" type="text" name="Command[input]"
                                           class="form-control"
                                           placeholder="Входящая команда:"
                                           value="{{old('input')}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="command_output" class="col-sm-2 control-label">Ответ:</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="Ответ:" id="command_output" type="text" name="Command[output]" value="{{old('output')}}">
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label for="command_vocalizer" class="col-sm-2 control-label">Голос:</label>--}}
                                {{--<div class="col-sm-10">--}}
                                    <input id="command_vocalizer" type="hidden" name="Command[vocalizer]" class="form-control"
                                           value="{{old('vocalizer')}}">
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label for="command_movement" class="col-sm-2 control-label">Движение:</label>
                                <div class="col-sm-10">
                                    <select id="command_movement" name="Command[movement_id]" class="form-control">
                                        <option value=0>-</option>
                                        @foreach ($movements as $movement)
                                            @php $selected = ($movement->id == old('movement_id')) ? "selected = 'selected'" : "" @endphp
                                            <option value= {{$movement->id}} {{$selected}}>{{$movement->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="/admin/commands" class="btn btn-default">Отмена</a>
                                    <button type="submit" class="btn btn-default">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
