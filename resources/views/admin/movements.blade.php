@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Базовые движения</div>
                    <div class="panel-body">
                        <a class="btn btn-primary btn-sm" href ="{{route('admin.movement.add')}}">Добавить базовое движение</a>
                        <br>
                        <br>
                        @if ($movements->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>Имя</th>
                                    <th style="width: 200px;">Действия</th>
                                </tr>
                                @foreach($movements as $movement)
                                <tr>
                                    <td>{{$movement->name}}</td>
                                    <td style="width: 200px;">
                                        <a class="btn btn-default btn-sm" href = "{{route('admin.movement.edit', ['id' => $movement->id])}}">Редактировать</a>
                                        <a class="btn btn-danger btn-sm" href = "{{route('admin.movement.delete', ['id' => $movement->id])}}">Удалить</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
