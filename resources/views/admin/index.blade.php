@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Панель управления</div>
                    <div class="panel-body">
                        <div class="button_block">
                            <a style="width: 200px;" class="btn btn-primary btn-sm" href = "{{route('admin.commands')}}">Базовые Диалоги</a>
                            <a style="width: 200px;" class="btn btn-primary btn-sm" href = "{{route('admin.movements')}}">Базовые Движения</a>
                            <a style="width: 200px;" class="btn btn-primary btn-sm" href = "{{route('admin.missed_commands.index')}}">Список запросов без ответа</a>
                            <a style="width: 200px;" class="btn btn-primary btn-sm" href = "{{route('admin.users.index')}}">Список пользователей</a>
                            <a style="width: 200px;" class="btn btn-primary btn-sm" href = "{{route('admin.updates.index')}}">Обновления</a>
                            <a style="width: 200px;" class="btn btn-primary btn-sm" href = "{{route('admin.audios.index')}}">Аудиофайлы</a>
                        </div>

                        <p class="lead">Таблица Роботов</p>
                        <div class="table_robots">
                            <table class="table">
                                <tr>
                                    <th>IMEI Робота</th>
                                    <th>Пользователь</th>
                                    <th style="width: 150px">Действия</th>
                                </tr>
                                @foreach($robots as $robot)
                                <tr>
                                    <td>{{$robot->imei}}</td>
                                    <td>{{$robot->user->email}}</td>
                                    <td style="width: 150px">
                                        <a style="width: 150px" href="javascript:void(0);" class="btn btn-default">Информация</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
