@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Добавить аудиофайл</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form method="POST" enctype="multipart/form-data" action="{{route("admin.audios.store")}}" role="form" class="form-horizontal">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="audio_name">Имя:</label>
                                <div class="col-sm-10">
                                    <input id="audio_name" type="text" name="Audio[name]" value="{{old('name')}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="audio_file">Файл:</label>
                                <div class="col-sm-10">
                                    <input id="audio_file" type="file" name="Audio[file]" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="{{route('admin.audios.index')}}" class="btn btn-default">Отмена</a>
                                    <button class="btn btn-default" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
