@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Добавить движение</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form method="POST" enctype="multipart/form-data" action="{{route("admin.movement.store")}}"
                              role="form" class="form-horizontal">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="movement_name">Имя:</label>
                                <div class="col-sm-10">
                                    <input id="movement_name" type="text" name="Movement[name]" value="{{old('name')}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="movement_file">Движение:</label>
                                <div class="col-sm-10">
                                <input id="movement_file" type="file" name="Movement[file]" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="/admin/movements" class="btn btn-default">Отмена</a>
                                    <button class="btn btn-default" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
