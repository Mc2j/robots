@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Список пользователей</div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Email</th>
                                <th style="width: 150px;">Действия</th>
                            </tr>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->email}}</td>
                                    <td style="width: 150px;" >
                                        <a class="btn btn-default btn-sm" href = "{{route('admin.users.detail', ['id' => $user->id])}}">Посмотреть информацию</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
