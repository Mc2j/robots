@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Пользователь {{$user->email}}</div>
                    <div class="panel-body">
                        <a href="{{route('admin.users.robots', ['id' => $user->id])}}">Роботы</a><br>
                        <a href="{{route('admin.users.commands', ['id' => $user->id])}}">Пользовательские диалоги</a><br>
                        <a href="{{route('admin.users.movements', ['id' => $user->id])}}">Пользовательские движения</a><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
