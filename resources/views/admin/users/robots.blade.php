@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Роботы пользователя</div>
                    <div class="panel-body">
                        <table width = "100%">
                            <tr>
                                <th>Imei робота</th>
                            </tr>
                            @foreach($robots as $robot)
                                <tr>
                                    <td>{{$robot->imei}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
