@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Диалоги пользователя</div>
                    <div class="panel-body">
                        <table width = "100%">
                            <tr>
                                <th>Входящая команда</th>
                                <th>Ответ</th>
                                <th>Голос</th>
                                <th>Движение</th>
                            </tr>
                            @foreach($commands as $command)
                                <tr>
                                    <td>{{$command->input}}</td>
                                    <td>{{$command->output}}</td>
                                    <td>{{$command->vocalizer}}</td>
                                    <td>
                                        @if ($command->movement)
                                            {{$command->movement->name}}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
