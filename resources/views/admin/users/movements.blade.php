@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Движения пользователя</div>
                    <div class="panel-body">
                        <table width = "100%">
                            <tr>
                                <th>Название</th>
                                <th>Файл</th>
                            </tr>
                            @foreach($movements as $movement)
                                <tr>
                                    <td>{{$movement->name}}</td>
                                    <td><a href ="{{$movement->file}}">Скачать</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
