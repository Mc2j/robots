@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Добавить обновление</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form method="POST" enctype="multipart/form-data" action="{{route("admin.updates.store")}}" role="form" class="form-horizontal">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="update_name">Имя пакета:</label>
                                <div class="col-sm-10">
                                    <input id="update_name" type="text" name="Update[name]" value="{{old('name')}}" class = "form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="update_file">Файл:</label>
                                <div class="col-sm-10">
                                <input id="update_file" type="file" name="Update[file]" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="update_robot">Робот:</label>
                                <div class="col-sm-10">
                                    <select name = "Update[robot_id]" id = "update_robot" class = "form-control">
                                        <option value = 0>Все</option>
                                        @foreach ($robots as $robot)
                                            @php $selected = ($robot->id == old('robot_id')) ? "selected = 'selected' " : "" @endphp
                                            <option value = {{$robot->id}} {{$selected}}>{{$robot->imei}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="{{route('admin.updates.index')}}" class="btn btn-default">Отмена</a>
                                    <button class="btn btn-default" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
