@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Список обновлений</div>
                    <div class="panel-body">
                        <a class="btn btn-primary btn-sm" href ="{{route('admin.updates.add')}}">Добавить обновление</a>
                        <br>
                        <br>
                        <table class="table">
                            <tr>
                                <th>Имя</th>
                                <th>Файл</th>
                                <th>Хэш</th>
                                <th>Создан</th>
                                <th>Робот</th>
                                <th style="width: 150px;">Действия</th>
                            </tr>
                            @foreach($updates as $update)
                                <tr>
                                    <td>{{$update->name}}</td>
                                    <td><a href = "{{$update->file}}">Скачать</a></td>
                                    <td>{{$update->hash}}</td>
                                    <td>{{$update->created_at}}</td>
                                    <td>
                                        @if ($update->robot !== null)
                                            {{$update->robot->imei}}
                                        @else
                                            Все
                                        @endif
                                    </td>
                                    <td style="width: 150px;" >
                                        <a class="btn btn-default btn-sm" href = "{{route('admin.updates.delete', ['id' => $update->id])}}">Удалить</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
