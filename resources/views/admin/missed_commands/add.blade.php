@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading lead">Добавить команду</div>
                    @if (!$errors->isEmpty())
                        <div class="error">
                            @foreach($errors->all() as $error)
                                {{$error}} <br>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-body">
                        <form method="POST" action="{{route("admin.missed_commands.store")}}" role="form"
                              class="form-horizontal">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="command_input">Входящая команда:</label>
                                <div class="col-sm-10">
                                    <input id="command_input" type="text" name="Command[input]" class="form-control"
                                           value="{{old('input', \Request::get('input'))}}">
                                </div>
                            </div>

                            <div class="form-group">
                            <label class="col-sm-2 control-label" for="command_output">Ответ:</label>
                                <div class="col-sm-10">
                                    <input placeholder="Внестите ответ:" id="command_output" type="text" name="Command[output]" value="{{old('output')}}" class="form-control">
                                </div>
                            </div>

                            {{--<label for="command_vocalizer">Голос:</label>--}}
                            <input id="command_vocalizer" type="hidden" name="Command[vocalizer]" value="1">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="command_movement">Движение:</label>
                                <div class="col-sm-10">
                                    <select id="command_movement" name="Command[movement_id]" class="form-control">
                                        <option value=0>-</option>
                                        @foreach ($movements as $movement)
                                            @php $selected = ($movement->id == old('movement_id')) ? "selected = 'selected'" : "" @endphp
                                            <option value= {{$movement->id}} {{$selected}}>{{$movement->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <a href="/admin/missed-commands" class="btn btn-default">Отмена</a>
                                    <button class="btn btn-default" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
