@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Диалоги без ответа</div>
                    <div class="panel-body">
                        @if ($missed_commands->count() > 0)
                        <table class="table">
                            <tr>
                                <th>Входящая команда</th>
                                <th style="width: 280px">Действия</th>
                            </tr>
                            @foreach($missed_commands as $command)
                                <tr>
                                    <td>{{$command->input}}</td>
                                    <td style="width: 280px" >
                                        <a class="btn btn-default btn-sm" href = "{{route('admin.missed_commands.add', ['input' => $command->input])}}">Добавить в общие</a>
                                        <a class="btn btn-danger btn-sm" href = "{{route('admin.missed_commands.delete', ['id' => $command->id])}}">Удалить из списка</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <br>
                        <a class="btn btn-danger btn-sm" href = "{{route('admin.missed_commands.delete_all')}}">Очистить</a>
                        @else
                            <p>Диалогов без ответа не найдено</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
