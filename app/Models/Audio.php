<?php

namespace App\Models;

use App\Traits\ScopeForUserTrait;

/**
 * Class Audio
 * @package App\Models
 *
 * @method static Movement forUser()
 * @method static Movement allForUser()
 * @method static Audio whereUserId($user_id)
 *
 * @mixin \Eloquent
 */

class Audio extends \Eloquent
{
    use ScopeForUserTrait;

    protected $table = 'audios';
    public $timestamps = false;
    protected $fillable = ['name', 'file', 'user_id'];
}