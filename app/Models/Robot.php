<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Robot
 * @package App\Models
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property integer $vocalizer
 * @property string $hello_message
 *
 * @method static Robot|null whereUserId($user_id)
 * @method static Robot|null whereToken($token)
 * @method static Robot|null whereImei($imei)
 *
 * @mixin \Eloquent
 */

class Robot extends Model
{
    protected $table = "robots";
    public $timestamps = false;
    protected $fillable = ['token', 'user_id', 'imei', 'vocalizer', 'hello_message'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}