<?php

namespace App\Models;

use App\Traits\ScopeForUserTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Command
 * @package App\Models
 *
 * @property integer $id
 * @property integer $vocalizer
 * @property integer $user_id
 * @property array $movement
 *
 * @method static Command|null forUser()
 * @method static Command|null allForUser()
 *
 * @method static Command|null whereId($id)
 * @method static Command|null whereInput($input)
 * @method static Command|null whereUserId($user_id)
 *
 * @mixin \Eloquent
 */

class Command extends Model
{
    use ScopeForUserTrait;

    protected $table = "commands";
    public $timestamps = false;
    protected $fillable = ['input', 'output', 'vocalizer', 'user_id', 'movement_id'];

    public static function getDefaultData()
    {
        return [
            'input' => '',
            'output' => '',
            'vocalizer' => 0,
            'movement_id' => 0,
            'user_id' => 0,
            'movement_name' => '',
        ];
    }

    public function movement()
    {
        return $this->belongsTo(Movement::class);
    }

    public function toArray()
    {
        $movement_name = $this->movement()->first(['name']);
        $movement_name = $movement_name->name ?? null;

        return array_merge(parent::toArray(), [
            'movement_name' => $movement_name,
        ]);
    }


}