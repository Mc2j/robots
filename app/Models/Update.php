<?php

namespace App\Models;

class Update extends \Eloquent
{
    protected $table = 'updates';
    protected $fillable = ['file', 'hash', 'updated_at', 'created_at', 'robot_id', 'name'];

    public function robot()
    {
        return $this->belongsTo(Robot::class);
    }
}