<?php

namespace App\Models;

/**
 * Class MissedCommandsLog
 * @package App\Models
 *
 * @method static MissedCommandsLog whereInput($input)
 *
 * @mixin \Eloquent
 */
class MissedCommandsLog extends \Eloquent
{
    protected $table = 'missed_commands_log';
    protected $fillable = ['input', 'robot_id', 'created_at', 'updated_at'];
}