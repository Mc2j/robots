<?php

namespace App\Models;

use App\Traits\ScopeForUserTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Movement
 * @package App\Models
 *
 * @property integer $id
 * @property integer $user_id
 *
 * @method static Movement forUser()
 * @method static Movement allForUser()
 * @method static Movement whereId($id)
 * @method static Movement whereUserId($user_id)
 *
 * @mixin \Eloquent
 */

class Movement extends Model
{
    use ScopeForUserTrait;

    protected $table = "movements";
    public $timestamps = false;
    protected $fillable = ['name', 'file', 'user_id'];
}