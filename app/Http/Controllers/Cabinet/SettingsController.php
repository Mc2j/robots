<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        return view("cabinet.settings.index");
    }

    public function save(Request $request)
    {
        return redirect()->route("home.settings.index");
    }
}
