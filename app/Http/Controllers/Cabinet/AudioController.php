<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\Audio;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AudioController
{
    public function index()
    {
        $custom_audios = Audio::forUser()->get();
        $audios = Audio::whereUserId(0)->get();

        return view('cabinet.audios.index', [
            'custom_audios' => $custom_audios,
            'audios' => $audios,
        ]);
    }

    public function add()
    {
        return view('cabinet.audios.add');
    }

    public function store(Request $request)
    {
        $data = $request->get('Audio');
        $file = $request->file();

        $errors = $this->validateRequest($data, $file);

        if (count($errors) == 0) {
            $movement = new Audio([
                'user_id' => \Auth::id(),
                'name' => $data['name'],
                'file' => $data['filename'],
            ]);
            $movement->save();

            return redirect()->route('home.audios.index');
        }

        return redirect()->route('home.audios.add')->withErrors($errors)->withInput($data);
    }

    public function edit($id)
    {
        $audio = $this->getAudio($id);

        return view('cabinet.audios.edit', [
            'audio' => $audio,
        ]);
    }

    public function change(Request $request, $id)
    {
        $audio = $this->getAudio($id);
        $data = $request->get('Audio');
        unset($data['filename']);
        $file = $request->file();

        $errors = $this->validateRequest($data, $file, false);

        if (count($errors) == 0) {
            $audio->fill([
                'name' => $data['name'],
            ]);

            if (isset($data['filename'])) {
                $audio->fill([
                    'file' => $data['filename'],
                ]);
            }

            $audio->save();

            return redirect()->route('home.audios.edit', [
                'id' => $id,
            ]);
        }

        return redirect()->route('home.audios.edit', [
            'id' => $id,
        ])->withErrors($errors)->withInput($data);
    }

    public function delete($id)
    {
        $audio = $this->getAudio($id);
        Audio::destroy($audio->id);
        return redirect()->route('home.audios.index');
    }

    protected function getAudio($id)
    {
        $audio = Audio::forUser()->whereId($id)->first();
        if (!$audio) {
            throw new NotFoundHttpException('Audio not found');
        }

        return $audio;
    }

    protected function validateRequest(&$data, $file, $required_file = true)
    {
        $errors = [];

        if (mb_strlen($data['name']) <= 0) {
            $errors[] = "Укажите имя";
        }

        if (count($errors) == 0 && isset($file['Audio']) && isset($file['Audio']['file'])) {
            $file = $file['Audio']['file'];
            /** @var UploadedFile $file */
            $filename = strtotime('now').mt_rand(10000, 99999).$file->getClientOriginalName();
            $link = \Storage::putFileAs('public/audios/users/'.$filename, $file, $file->getClientOriginalName());
            $data['filename'] = str_replace('public/', '/storage/', $link);
        } else {
            if ($required_file) {
                $errors[] = 'Прикрепите файл';
            }
        }

        return $errors;
    }
}
