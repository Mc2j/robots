<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Robot;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RobotsController extends Controller
{
    public function add()
    {
        return view('cabinet.robot.add');
    }

    public function store(Request $request)
    {
        $data = $request->get('Robot');
        $errors = $this->validateRobotRequest($data);

        if (count($errors) == 0) {

            $robot = Robot::whereImei($data['imei'])->first();
            if (!$robot) {
                $data['user_id'] = \Auth::id();
                $data['token'] = \Hash::make($data['imei']);

                $robot = new Robot();
                $robot->fill($data);
                $robot->save();

                return redirect()->route('home.index');
            }

            $errors[] = "Робот с таким imei уже существует";
        }

        return redirect()->route('home.robots.add')->withErrors($errors)->withInput($data);
    }

    public function edit($id)
    {
        $robot = $this->getRobot($id);

        return view('cabinet.robot.edit', [
            'robot' => $robot,
        ]);
    }

    public function change(Request $request, $id)
    {
        $robot = $this->getRobot($id);
        $data = $request->get('Robot');

        $errors = $this->validateRobotRequest($data);

        if (count($errors) == 0) {
            $robot->fill($data);
            $robot->save();
        }

        return redirect()->route('home.robots.edit', [
            'id' => $robot->id,
        ])->withErrors($errors)->withInput($data);
    }

    public function delete($id)
    {
        $robot = $this->getRobot($id);
        Robot::destroy($robot->id);
        return redirect()->route('home.index');
    }

    protected function getRobot($id)
    {
        $robot = Robot::find($id);
        if ($robot->user_id != \Auth::id()) {
            throw new NotFoundHttpException('Robot not found');
        }

        return $robot;
    }

    protected function validateRobotRequest($data)
    {
        $errors = [];
        if (mb_strlen($data['imei']) <= 0) {
            $errors[] = 'Укажите IMEI';
        }
        if (intval($data['vocalizer']) <= 0) {
            $errors[] = 'Укажите голос (число)';
        }
        if (mb_strlen($data['hello_message']) <= 0) {
            $errors[] = 'Укажите слово активации';
        }

        return $errors;
    }
}