<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Movement;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MovementsController extends Controller
{
    public function index()
    {
        $custom_movements = Movement::forUser()->get();
        $movements = Movement::whereUserId(0)->get();

        return view('cabinet.movements.index', [
            'custom_movements' => $custom_movements,
            'movements' => $movements,
        ]);
    }

    public function add()
    {
        return view('cabinet.movements.add');
    }

    public function store(Request $request)
    {
        $data = $request->get('Movement');
        $file = $request->file();

        $errors = $this->validateMovementRequest($data, $file);

        if (count($errors) == 0) {
            $movement = new Movement([
                'user_id' => \Auth::id(),
                'name' => $data['name'],
                'file' => $data['filename'],
            ]);
            $movement->save();

            return redirect()->route('home.movements.index');
        }

        return redirect()->route('home.movements.add')->withErrors($errors)->withInput($data);
    }

    public function edit($id)
    {
        $movement = $this->getMovement($id);

        return view('cabinet.movements.edit', [
            'movement' => $movement,
        ]);
    }

    public function change(Request $request, $id)
    {
        $movement = $this->getMovement($id);
        $data = $request->get('Movement');
        unset($data['filename']);
        $file = $request->file();

        $errors = $this->validateMovementRequest($data, $file, false);

        if (count($errors) == 0) {
            $movement->fill([
                'name' => $data['name'],
            ]);

            if (isset($data['filename'])) {
                $movement->fill([
                    'file' => $data['filename'],
                ]);
            }

            $movement->save();

            return redirect()->route('home.movements.edit', [
                'id' => $movement->id,
            ]);
        }

        return redirect()->route('home.movements.edit', [
            'id' => $movement->id,
        ])->withErrors($errors)->withInput($data);
    }

    public function delete($id)
    {
        $movement = $this->getMovement($id);
        Movement::destroy($movement->id);
        return redirect()->route('home.movements.index');
    }

    protected function getMovement($id)
    {
        $movement = Movement::forUser()->whereId($id)->first();
        if (!$movement) {
            throw new NotFoundHttpException('Movement not found');
        }

        return $movement;
    }

    protected function validateMovementRequest(&$data, $file, $required_file = true)
    {
        $errors = [];

        if (mb_strlen($data['name']) <= 0) {
            $errors[] = "Укажите имя";
        }

        if (count($errors) == 0 && isset($file['Movement']) && isset($file['Movement']['file'])) {
            $file = $file['Movement']['file'];
            /** @var UploadedFile $file */
            $filename = strtotime('now').mt_rand(10000, 99999).$file->getClientOriginalName();
            $link = \Storage::putFileAs('public/movements/users/'.$filename, $file, $file->getClientOriginalName());
            $data['filename'] = str_replace('public/', '/storage/', $link);
        } else {
            if ($required_file) {
                $errors[] = 'Прикрепите файл';
            }
        }

        return $errors;
    }
}
