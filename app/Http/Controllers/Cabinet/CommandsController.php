<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Command;
use App\Models\Movement;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommandsController extends Controller
{
    public function index()
    {
        $custom_commands = Command::forUser()->get();
        $commands = Command::whereUserId(0)->get();

        return view('cabinet.commands.index', [
            'custom_commands' => $custom_commands,
            'commands' => $commands,
        ]);
    }

    public function add()
    {
        $movements = Movement::allForUser()->get();

        return view('cabinet.commands.add', [
            'movements' => $movements,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->get('Command');
        $errors = $this->validateCommandRequest($data);

        if (count($errors) == 0) {
            $data['user_id'] = \Auth::id();

            $custom_command = new Command();
            $custom_command->fill($data);
            $custom_command->save();

            return redirect()->route('home.commands.index');
        }

        return redirect()->route('home.commands.add')->withErrors($errors)->withInput($data);
    }

    public function edit($id)
    {
        $command = $this->getCommand($id);
        $movements = Movement::allForUser()->get();
        return view('cabinet.commands.edit', [
            'command' => $command,
            'movements' => $movements,
        ]);
    }

    public function change(Request $request, $id)
    {
        $custom_command = $this->getCommand($id);
        $data = $request->get('Command');

        $errors = $this->validateCommandRequest($data);

        if (count($errors) == 0) {
            $custom_command->fill($data);
            $custom_command->save();
        }

        return redirect()->route('home.commands.edit', [
            'id' => $id
        ])->withErrors($errors)->withInput($data);
    }

    public function delete($id)
    {
        $command = $this->getCommand($id);
        Command::destroy($command->id);
        return redirect()->route('home.commands.index');
    }

    protected function getCommand($id)
    {
        $command = Command::forUser()->whereId($id)->first();
        if (!$command) {
            throw new NotFoundHttpException('Command not found');
        }

        return $command;
    }

    protected function validateCommandRequest($data)
    {
        $errors = [];
        if (mb_strlen($data['input']) <= 0) {
            $errors[] = 'Укажите входящую команду';
        }
        if (mb_strlen($data['output']) <= 0) {
            $errors[] = 'Укажите ответ';
        }
        if (mb_strlen($data['vocalizer']) <= 0) {
            $errors[] = 'Укажите голос';
        }

        if ($data['movement_id'] > 0) {
            $movement = Movement::allForUser()->whereId($data['movement_id'])->first();
            if (!$movement) {
                $errors[] = 'Укажите корректное движение';
            }
        }

        return $errors;
    }
}
