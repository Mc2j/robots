<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Robot;

class DefaultController extends Controller
{
    public function index()
    {
        $user_id = \Auth::user()->id;
        $robots = Robot::whereUserId($user_id)->get();

        return view('cabinet.index', [
            'robots' => $robots,
        ]);
    }
}
