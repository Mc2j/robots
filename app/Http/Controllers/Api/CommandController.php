<?php

namespace App\Http\Controllers\Api;

use App\Models\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\ParameterBag;

class CommandController
{
    public function index()
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        $commands = Command::whereIn('user_id', [0, $user->id])->get();
        return response()->json($commands, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function storeCommands(Request $request)
    {
        $response = [];
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        /** @var ParameterBag $json */
        $json = $request->json();

        if ($json->count() > 0) {
            foreach ($json->get('List') as $command) {
                try {
                    $command['user_id'] = $user->id;
                    $command['output'] = array_get($command, 'output', '');
                    $command['vocalizer'] = array_get($command, 'vocalizer', 0);
                    $command['movement_id'] = array_get($command, 'movement_id', 0);

                    $validator = \Validator::make($command, [
                        'input' => [
                            Rule::unique('commands')->where(function (Builder $query) use ($user) {
                                return $query->where('user_id', $user->id);
                            }),
                            'required',
                        ],
                    ]);

                    if (!$validator->passes()) {
                        throw new \Exception($validator->errors()->first());
                    }

                    $command = new Command($command);
                    $command->save();

                    $response[] = response()->json([
                        'id' => $command->id,
                    ]);
                } catch (\Exception $e) {
                    $response[] = response()->json()->withException($e);
                }
            }
        }

        return response()->json($response, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function delete($id)
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        $command = Command::find($id);
        if ($command->user_id != $user->id) {
            return response()->json([
                'success' => false,
            ], 500);
        }

        Command::destroy($id);
        return response()->json([
            'success' => true
        ]);
    }
}