<?php

namespace App\Http\Controllers\Api;

use App\Models\Movement;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MovementController
{
    public function index()
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        $movements = Movement::whereIn('user_id', [0, $user->id])->get();
        return response()->json($movements, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function store(Request $request)
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        try {
            $data = $request->json()->all();

            $validator = \Validator::make($data, [
                'name' => [
                    Rule::unique('movements')->where(function (Builder $query) use ($user) {
                        return $query->where('user_id', $user->id);
                    }),
                    'required',
                ],
                'file' => 'required',
            ]);

            if (!$validator->passes()) {
                throw new \Exception($validator->errors()->first(), 500);
            }

            $content = base64_decode($data['file']);

            $dir = base_path('storage/app/public/movements/api/').$user->id;
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }

            \File::put($dir."/".$data['name'].".erf", $content);
            $url = "/storage/movements/api/".$user->id."/".$data['name'].".erf";
            $movement = new Movement([
                'name' => $data['name'],
                'file' => $url,
                'user_id' => $user->id,
            ]);
            $movement->save();

            return response()->json([
                'success' => true,
                'file' => $url,
            ], 200, [], JSON_UNESCAPED_UNICODE);

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    public function delete($id)
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        $movement = Movement::find($id);
        if ($movement->user_id != $user->id) {
            return response()->json([
                'success' => false,
            ], 500);
        }

        Movement::destroy($id);
        return response()->json([
            'success' => true
        ]);
    }
}