<?php

namespace App\Http\Controllers\Api;

use App\Models\Robot;
use Illuminate\Http\Request;

class RobotController
{
    public function index()
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        $robots = Robot::whereUserId($user->id)->get();
        return response()->json($robots);
    }

    public function create(Request $request)
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);
        $data = $request->all();

        $validator = \Validator::make($data, [
            'imei' => 'required|unique:robots'
        ]);

        if (!$validator->passes()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->all(),
            ], 500);
        }

        $data['token'] = \Hash::make($data['imei']);
        $data['user_id'] = $user->id;
        $data['hello_message'] = array_get($data, 'hello_message', config('robots.hello_message'));

        $robot = new Robot($data);
        $robot->save();

        return response()->json([
            'success' => true
        ]);
    }

    public function delete($id)
    {
        $token = \Request::bearerToken();
        $user = \JWTAuth::toUser($token);

        $robot = Robot::find($id);
        if ($robot->user_id != $user->id) {
            return response()->json([
                'success' => false,
            ], 500);
        }

        Robot::destroy($id);
        return response()->json([
            'success' => true
        ]);
    }
}