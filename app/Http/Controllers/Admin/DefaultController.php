<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Command;
use App\Models\Movement;
use App\Models\Robot;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    public function index()
    {
        $robots = Robot::get();
        return view('admin.index', [
            'robots' => $robots,
        ]);
    }

    public function commands()
    {
        $commands = Command::whereUserId(0)->get();
        return view('admin.commands', [
            'commands' => $commands,
        ]);
    }

    public function addCommand()
    {
        $movements = Movement::whereUserId(0)->get();
        return view('admin.command_add', [
            'movements' => $movements,
        ]);
    }

    public function storeCommand(Request $request)
    {
        $data = $request->get('Command');
        $errors = $this->validateCommandRequest($data);

        if (count($errors) == 0) {
            $data['user_id'] = 0;
            $command = new Command();
            $command->fill($data);
            $command->vocalizer = 0;
            $command->save();

            return redirect()->route('admin.commands');
        }

        return redirect()->route('admin.command.add')->withErrors($errors)->withInput($data);
    }

    public function editCommand($id)
    {
        $command = $this->getCommand($id);
        $movements = Movement::whereUserId(0)->get();

        return view('admin.command_edit', [
            'command' => $command,
            'movements' => $movements,
        ]);
    }

    public function changeCommand(Request $request, $id)
    {
        $command = $this->getCommand($id);

        $data = $request->get('Command');
        $errors = $this->validateCommandRequest($data);

        if (count($errors) == 0) {
            $command->fill($data);
            $command->save();
        }

        return redirect()->route('admin.command.edit', [
            'id' => $command->id,
        ])->withErrors($errors)->withInput($data);
    }

    public function deleteCommand($id)
    {
        $command = $this->getCommand($id);
        Command::destroy($command->id);
        return redirect()->route('admin.commands');
    }

    protected function getCommand($id)
    {
        $command = Command::whereId($id)->whereUserId(0)->first();
        if (!$command) {
            throw new \Exception('Command not found', 404);
        }

        return $command;
    }

    protected function validateCommandRequest($data)
    {
        $errors = [];

        if (mb_strlen($data['input']) <= 0) {
            $errors[] = 'Укажите входящую команду';
        }
        if (mb_strlen($data['output']) <= 0) {
            $errors[] = 'Укажите ответ';
        }

        return $errors;
    }

    public function movements()
    {
        $movements = Movement::whereUserId(0)->get();

        return view('admin.movements', [
            'movements' => $movements,
        ]);
    }

    public function addMovement()
    {
        return view('admin.movement_add');
    }

    public function storeMovement(Request $request)
    {
        $data = $request->get('Movement');
        $file = $request->file();

        $errors = $this->validateMovementRequest($data, $file);

        if (count($errors) == 0) {
            $custom_movement = new Movement([
                'user_id' => 0,
                'name' => $data['name'],
                'file' => $data['filename'],
            ]);
            $custom_movement->save();

            return redirect()->route('admin.movements');

        }

        return redirect()->route('admin.movement.add')->withErrors($errors)->withInput($data);
    }

    public function editMovement($movement_id)
    {
        $movement = $this->getMovement($movement_id);

        return view('admin.movement_edit', [
            'movement' => $movement,
        ]);
    }

    public function changeMovement(Request $request, $movement_id)
    {
        $movement = $this->getMovement($movement_id);
        $data = $request->get('Movement');
        unset($data['filename']);
        $file = $request->file();

        $errors = $this->validateMovementRequest($data, $file, false);

        if (count($errors) == 0) {
            $movement = Movement::find($movement_id);
            $movement->fill([
                'name' => $data['name'],
            ]);

            if (isset($data['filename'])) {
                $movement->fill([
                    'file' => $data['filename'],
                ]);
            }

            $movement->save();

            return redirect()->route('admin.movement.edit', [
                'id' => $movement->id,
            ]);
        }

        return redirect()->route('admin.movement.edit', [
            'id' => $movement->id
        ])->withErrors($errors)->withInput($data);
    }

    public function deleteMovement($movement_id)
    {
        $movement = $this->getMovement($movement_id);
        Movement::destroy($movement->id);
        return redirect()->route('admin.movements');
    }

    protected function validateMovementRequest(&$data, $file, $required_file = true)
    {
        $errors = [];

        $validator = \Validator::make($data, [
            'name' => [
                Rule::unique('movements')->where(function (Builder $query) {
                    return $query->where('user_id', 0);
                }),
                'required'
            ],
        ], [
            "name.unique" => "Такое имя уже существует",
            "name.required" => "Укажите имя",
        ]);

        if (!$validator->passes()) {
            $errors = $validator->messages()->all();
        }

        if (count($errors) == 0 && isset($file['Movement']) && isset($file['Movement']['file'])) {
            $file = $file['Movement']['file'];
            /** @var UploadedFile $file */
            $filename = strtotime('now').mt_rand(10000, 99999).$file->getClientOriginalName();
            $link = \Storage::putFileAs('public/movements/'.$filename, $file, $file->getClientOriginalName());
            $data['filename'] = str_replace('public/', '/storage/', $link);
        } else {
            if ($required_file) {
                $errors[] = 'Прикрепите файл';
            }
        }

        return $errors;
    }

    protected function getMovement($movement_id)
    {
        $movement = Movement::whereUserId(0)->whereId($movement_id)->first();
        if (!$movement) {
            throw new NotFoundHttpException('Movement not found');
        }

        return $movement;
    }
}