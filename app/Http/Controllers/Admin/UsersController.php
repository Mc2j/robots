<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Command;
use App\Models\Movement;
use App\Models\Robot;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', [
            'users' => $users,
        ]);
    }

    public function detail($id)
    {
        $user = $this->findUser($id);

        return view('admin.users.detail', [
            'user' => $user,
        ]);
    }

    public function robots($id)
    {
        $user = $this->findUser($id);
        $robots = Robot::whereUserId($id)->get();

        return view('admin.users.robots', [
            'user' => $user,
            'robots' => $robots,
        ]);
    }

    public function commands($id)
    {
        $user = $this->findUser($id);
        $commands = Command::whereUserId($user->id)->get();

        return view('admin.users.commands', [
            'user' => $user,
            'commands' => $commands,
        ]);
    }

    public function movements($id)
    {
        $user = $this->findUser($id);
        $movements = Movement::whereUserId($user->id)->get();

        return view('admin.users.movements', [
            'user' => $user,
            'movements' => $movements,
        ]);
    }

    protected function findUser($id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new \Exception('User not found');
        }
        return $user;
    }
}