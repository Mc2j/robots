<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Command;
use App\Models\MissedCommandsLog;
use App\Models\Movement;
use Illuminate\Http\Request;

class MissedCommandsController extends Controller
{
    public function index()
    {
        $missed_commands = MissedCommandsLog::get();
        return view('admin.missed_commands.index', [
            'missed_commands' => $missed_commands,
        ]);
    }

    public function add()
    {
        $movements = Movement::whereUserId(0)->get();
        return view('admin.missed_commands.add', [
            'movements' => $movements,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->get('Command');
        $errors = $this->validateCommandRequest($data);

        if (count($errors) == 0) {
            $data['user_id'] = 0;
            $command = new Command();
            $command->fill($data);
            $command->save();

            $old_missed_commands = MissedCommandsLog::whereInput($data['input'])->get();
            foreach ($old_missed_commands as $old_missed_command) {
                MissedCommandsLog::destroy($old_missed_command->id);
            }

            return redirect()->route('admin.missed_commands.index');
        }

        return redirect()->route('admin.missed_commands.add')->withErrors($errors)->withInput($data);
    }

    protected function validateCommandRequest($data)
    {
        $errors = [];

        if (mb_strlen($data['input']) <= 0) {
            $errors[] = 'Укажите входящую команду';
        }
        if (mb_strlen($data['output']) <= 0) {
            $errors[] = 'Укажите ответ';
        }
        if (mb_strlen($data['vocalizer']) <= 0) {
            $errors[] = 'Укажите голос';
        }

        return $errors;
    }

    public function delete($id)
    {
        MissedCommandsLog::destroy($id);
        return redirect()->route('admin.missed_commands.index');
    }

    public function deleteAll()
    {
        MissedCommandsLog::truncate();
        return redirect()->route('admin.missed_commands.index');
    }
}