<?php

namespace App\Http\Controllers\Admin;

use App\Models\Robot;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class UpdatesController
{
    public function index()
    {
        $updates = Update::orderBy('created_at', 'desc')->get();

        return view('admin.updates.index', [
            'updates' => $updates,
        ]);
    }

    public function add()
    {
        $robots = Robot::get();
        return view('admin.updates.add', [
            'robots' => $robots,
        ]);
    }

    public function store(Request $request)
    {
        $errors = [];
        $link = "";

        $data = $request->get('Update');
        $file = $request->file();

        $validator = \Validator::make($data, [
            'name' => 'required',
        ]);

        if (!$validator->passes()) {
            $errors = $validator->errors()->toArray();
        }

        if (empty($errors) && isset($file['Update']) && isset($file['Update']['file'])) {
            $file = $file['Update']['file'];
            /** @var UploadedFile $file */
            $name = $file->getClientOriginalName();
            $filename = strtotime('now').mt_rand(10000, 99999).$name;
            $link = \Storage::putFileAs('public/updates/'.$filename, $file, $name);
            $data['filename'] = str_replace('public/', '/storage/', $link);
        } else {
            $errors[] = 'Прикрепите файл';
        }

        if (count($errors) == 0) {
            $update = new Update([
                'file' => $data['filename'],
                'robot_id' => intval($data['robot_id']),
                'hash' => crc32(\File::get(storage_path()."/app/".$link)),
                'name' => $data['name'],
            ]);
            $update->save();

            return redirect()->route('admin.updates.index');

        }

        return redirect()->route('admin.updates.add')->withErrors($errors)->withInput($data);
    }

    public function delete($id)
    {
        Update::destroy($id);
        return redirect()->route('admin.updates.index');
    }
}