<?php

namespace App\Http\Controllers\Admin;

use App\Models\Audio;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

class AudioController
{
    public function index()
    {
        $audios = Audio::whereUserId(0)->get();

        return view('admin.audios.index', [
            'audios' => $audios,
        ]);
    }

    public function add()
    {
        return view('admin.audios.add');
    }

    public function store(Request $request)
    {
        $data = $request->get('Audio');
        $file = $request->file();

        $errors = $this->validateRequest($data, $file);

        if (count($errors) == 0) {
            $audio = new Audio([
                'user_id' => 0,
                'name' => $data['name'],
                'file' => $data['filename'],
            ]);
            $audio->save();

            return redirect()->route('admin.audios.index');

        }

        return redirect()->route('admin.audios.add')->withErrors($errors)->withInput($data);
    }

    public function edit($id)
    {
        $audio = Audio::find($id);

        return view('admin.audios.edit', [
            'audio' => $audio,
        ]);
    }
    public function change(Request $request, $id)
    {
        $audio = Audio::find($id);
        $data = $request->get('Audio');
        unset($data['filename']);
        $file = $request->file();

        $errors = $this->validateRequest($data, $file, false);

        if (count($errors) == 0) {
            $audio->fill([
                'name' => $data['name'],
            ]);

            if (isset($data['filename'])) {
                $audio->fill([
                    'file' => $data['filename'],
                ]);
            }

            $audio->save();

            return redirect()->route('admin.audios.edit', [
                'id' => $id,
            ]);
        }

        return redirect()->route('admin.audios.edit', [
            'id' => $id,
        ])->withErrors($errors)->withInput($data);
    }

    public function delete($id)
    {
        Audio::destroy($id);
        return redirect()->route('admin.audios.index');
    }

    protected function validateRequest(&$data, $file, $required_file = true)
    {
        $errors = [];

        $validator = \Validator::make($data, [
            'name' => ['required'],
        ], [
            "name.required" => "Укажите имя",
        ]);

        if (!$validator->passes()) {
            $errors = $validator->messages()->all();
        }

        if (count($errors) == 0 && isset($file['Audio']) && isset($file['Audio']['file'])) {
            $file = $file['Audio']['file'];
            /** @var UploadedFile $file */
            $filename = strtotime('now').mt_rand(10000, 99999).$file->getClientOriginalName();
            $link = \Storage::putFileAs('public/audios/'.$filename, $file, $file->getClientOriginalName());
            $data['filename'] = str_replace('public/', '/storage/', $link);
        } else {
            if ($required_file) {
                $errors[] = 'Прикрепите файл';
            }
        }

        return $errors;
    }
}