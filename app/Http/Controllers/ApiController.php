<?php

namespace App\Http\Controllers;

use App\Models\Audio;
use App\Models\Command;
use App\Models\MissedCommandsLog;
use App\Models\Movement;
use App\Models\Pose;
use App\Models\Robot;
use App\Models\Update;
use App\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = \JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function search()
    {
        $input = \Request::get('input');
        $robot_token = \Request::bearerToken();

        $robot = Robot::whereToken($robot_token)->first();

        if (!$robot) {
            return response()->json([
                'error' => 'Robot not found',
            ], 404);
        }

        $user = User::find($robot->user_id);
        if (!$user) {
            return response()->json([
                'error' => 'User not found',
            ], 404);
        }

        $command = Command::whereInput($input)->whereUserId($robot->user_id)->first();
        if (!$command) {
            $command = Command::whereInput($input)->whereUserId(0)->first();
            if (!$command) {

                $missed_command_log = new MissedCommandsLog([
                    'input' => $input,
                    'robot_id' => $robot->id,
                ]);
                $missed_command_log->save();

                return response()->json([
                    'error' => 'Command not found',
                ], 404);
            }
        }

        if ($robot->vocalizer > 0) {
            $command->vocalizer = $robot->vocalizer;
        }

        return response()->json($command, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function getToken()
    {
        $imei = \Request::get('imei');
        $robot = Robot::whereImei($imei)->first();
        if (!$robot) {
            return response()->json([
                'error' => 'Robot not found',
            ], 404);
        }

        return response()->json([
            'token' => $robot->token,
        ]);
    }

    public function getCommandsFromRobot()
    {
        $robot_token = \Request::bearerToken();
        $ids = [0];
        $vocalizer = 0;
        $hello_command = [];

        if ($robot_token) {
            $robot = Robot::whereToken($robot_token)->first();
            if ($robot) {
                $ids[] = $robot->user_id;
                $vocalizer = $robot->vocalizer;

                $hello_command = Command::getDefaultData();
                $hello_command['input'] = $robot->hello_message;
                $hello_command['user_id'] = $robot->user_id;
                $hello_command['hello'] = true;
            }
        }

        $commands = Command::whereIn('user_id', $ids)->get()->toArray();
        $commands = array_merge($commands, [$hello_command]);

        if ($vocalizer > 0) {
            array_walk($commands, function(&$command) use ($vocalizer) {
                $command['vocalizer'] = $vocalizer;
            });
        }

        array_walk($commands, function(&$command) {
            $command['hello'] = array_get($command, 'hello', false);
        });

        return response()->json($commands, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function getMovementsFromRobot()
    {
        $robot_token = \Request::bearerToken();
        $robot_ids = [0];

        if ($robot_token) {
            $robot = Robot::whereToken($robot_token)->first();
            if ($robot) {
                $robot_ids[] = $robot->user_id;
            }
        }

        $movements = Movement::whereIn('user_id', $robot_ids)->get();

        return response()->json($movements, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function addCommands(Request $request)
    {
        $response = [];

        $robot_token = \Request::bearerToken();
        if (!$robot_token) {
            return response()->json([
                'success' => false,
            ], 500);
        }

        $robot = Robot::whereToken($robot_token)->first();
        if (!$robot) {
            return response()->json([
                'success' => false,
            ], 500);
        }

        /** @var ParameterBag $json */
        $json = $request->json();
        if ($json->count() > 0) {
            foreach ($json->get('List') as $command) {

                try {
                    $command->user_id = $robot->user_id;
                    $command->vocalizer = $robot->vocalizer;
                    $command = new Command($command);
                    $command->save();

                    $response[] = response()->json([
                        'id' => $command->id,
                    ]);
                } catch (\Exception $e) {
                    $response[] = response()->json()->withException($e);
                }
            }
        }

        return response()->json($response, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function poses()
    {
        $poses = Pose::get();
        return response()->json($poses, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function storeMissedCommand(Request $request)
    {
        $robot_token = \Request::bearerToken();

        try {
            if (!$robot_token) {
                throw new \Exception('Missed token', 500);
            }

            $robot = Robot::whereToken($robot_token)->first();
            if (!$robot) {
                throw new \Exception('Token not found', 404);
            }

            $data = $request->json()->all();
            $validator = \Validator::make($data, [
                'input' => [
                    Rule::unique('missed_commands_log')->where(function (Builder $query) use ($robot) {
                        return $query->where('robot_id', $robot->id);
                    }),
                    'required',
                ],
            ]);

            if (!$validator->passes()) {
                throw new \Exception("Validator fails", 500);
            }

            $missed_commands = new MissedCommandsLog([
                'input' => $data['input'],
                'robot_id' => $robot->id,
            ]);
            $missed_commands->save();

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function storeMovement(Request $request)
    {
        try {
            $data = $request->json()->all();

            $validator = \Validator::make($data, [
                'name' => [
                    Rule::unique('movements')->where(function (Builder $query) {
                        return $query->where('user_id', 0);
                    }),
                    'required',
                ],
                'file' => 'required',
            ]);

            if (!$validator->passes()) {
                throw new \Exception($validator->errors()->all()[0], 500);
            }

            $file = base64_decode($data['file']);
            $fp = fopen(base_path('storage/app/public/movements/').$data['name'].".erf", 'a+');
            fwrite($fp, $file);
            fclose($fp);

            $movement = new Movement([
                'name' => $data['name'],
                'file' => 'storage/movements/api/'.$data['name'].".erf",
                'user_id' => 0,
            ]);
            $movement->save();

            return response()->json([
                'success' => true,
                'file' => 'storage/movements/'.$data['name'].'.erf',
            ], 200, [], JSON_UNESCAPED_UNICODE);

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    public function updates()
    {
        $ids = [0];
        $robot_token = \Request::bearerToken();
        if ($robot_token) {
            $robot = Robot::whereToken($robot_token)->first();
            if ($robot !== null) {
                $ids[] = $robot->id;
            }
        }

        $updates = Update::whereIn('robot_id', $ids)->get();
        return response()->json($updates, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function audios()
    {
        $ids = [0];
        $robot_token = \Request::bearerToken();
        if ($robot_token) {
            $robot = Robot::whereToken($robot_token)->first();
            if ($robot !== null) {
                $ids[] = $robot->user_id;
            }
        }

        $audios = Audio::whereIn('user_id', $ids)->get();
        return response()->json($audios, 200, [], JSON_UNESCAPED_UNICODE);
    }
}