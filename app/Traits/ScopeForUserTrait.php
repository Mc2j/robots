<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait ScopeForUserTrait
{
    public function scopeForUser(Builder $query)
    {
        $this->checkAuth();
        $query->where('user_id', \Auth::id());
    }

    public function scopeAllForUser(Builder $query)
    {
        $this->checkAuth();
        return $query->whereIn('user_id', [0, \Auth::id()]);
    }

    protected function checkAuth()
    {
        if (!\Auth::user()) {
            throw new \Exception("Unauthorized", 401);
        }
    }
}